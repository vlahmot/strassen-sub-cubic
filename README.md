This is an implementation of Strassen's sub-cubic matrix multiplication algorithm. 
I first read about the algorithm on [Wikipedia](http://en.wikipedia.org/wiki/Strassen_algorithm) and used both Wikipedia and  [here](http://martin-thoma.com/strassen-algorithm-in-python-java-cpp/)
as resources.

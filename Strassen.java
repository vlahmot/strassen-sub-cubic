
	
	/*
	 * NOTE: Requires that A,B have dimensions (2^n, 2^n)
	 * This algorithm recursively computes A X B
	 * I used http://en.wikipedia.org/wiki/Strassen_algorithm for the general algorithm
	 * and http://martin-thoma.com/strassen-algorithm-in-python-java-cpp/ for help  with the implementation.
	 */
	public static int[][] strassensAlgorithm(int[][] A, int[][] B)
	{
		int n = A.length;
	    if (n <= 2) {  // 2 here can be changed to determine when to use the generic algorithm
	        return ikjAlgorithm(A, B);
	    }else{
		
		int blockSize = n/2;
		
		//Break each matrix down in to 4 sub matrices for recursion
		//The division is:
		//	A = |a0,a1|   B = |b0,b1|  C = |c0,c1|
		//		|a2,a3|		  |b2,b3|      |c2,c3|
		// 
		// Where a0 is the sub-matrix that includes elements [0-n/2) (in both the row/column).
		// Where a1 is the sub-matrix that includes elements [n/2-n) in the row and [0-n/2) in the column.
		// Where a2 is the sub-matrix that includes elements [0-n/2) in the row and [n/2-n) in the column.
		// Where a3 is the sub-matrix that includes elements [n/2-n) (in both the row/column).
		// The same applies for B and C
		
		//Then c0 = (a0)b0 + (a1)b2
		//     c1 = (a0)b1 + (a1)b3
		//     c2 = (a2)b0 + (a3)b2
		//     c3 = (a2)b1 + (a3)b3
				
		//Initialize the sub-matrices of A and B defined above
		int[][] a0 = new int[blockSize][blockSize];
		int[][] a1 = new int[blockSize][blockSize];
		int[][] a2 = new int[blockSize][blockSize];
		int[][] a3 = new int[blockSize][blockSize];
		
		int[][] b0 = new int[blockSize][blockSize];
		int[][] b1 = new int[blockSize][blockSize];
		int[][] b2 = new int[blockSize][blockSize];
		int[][] b3 = new int[blockSize][blockSize];
		
		for (int i = 0; i < blockSize; i++) {
			for (int j = 0; j < blockSize; j++) {
			//Copy values over to the blocks according to definition above
			a0[i][j] = A[i][j];
			a1[i][j] = A[i][blockSize + j];
			a2[i][j] = A[i + blockSize][j];
			a3[i][j] = A[i + blockSize][j + blockSize];
			
			b0[i][j] = B[i][j];
			b1[i][j] = B[i][blockSize + j];
			b2[i][j] = B[i + blockSize][j];
			b3[i][j] = B[i + blockSize][j + blockSize];
			}
		}
		
		//Now let m1-m7 be new matrices defined as:
		//m1 = (a0 + a3)(b0 + b3)
		//m2 = (a2 + a3)b0
		//m3 = a0(b1 - b3)
		//m4 = a3(b2 - b0)
		//m5 = (a0 + a1)b3
		//m6 = (a2 - a0)(b0 + b2)
		//m7 = (a1 - a3)(b2 + b3)
		
		//Calculate m1-m7
		
		//m1 = (a0 + a3)(b0 + b3)
		int[][] m1a = add(a0,a3);
		int[][] m1b = add(b0,b3);
		int[][] m1 = strassensAlgorithm(m1a,m1b);
		
		//m2 = (a2 + a3)b0
		int[][] m2a = add(a2,a3);
		int[][] m2 = strassensAlgorithm(m2a,b0);
		
		//m3 = a0(b1 - b3)

		int[][] m3b = subtract(b1,b3);
		int[][] m3 = strassensAlgorithm(a0,m3b);
		
		//m4 = a3(b2 - b0)
		int[][] m4b = subtract(b2,b0);
		int[][] m4 = strassensAlgorithm(a3,m4b);
		
		//m5 = (a0 + a1)b3

		int[][] m5a = add(a0,a1);
		int[][] m5 = strassensAlgorithm(m5a,b3);
		
		//m6 = (a2 - a0)(b0 + b2)
		int[][] m6a = subtract(a2,a0);
		int[][] m6b = add(b0,b2);
		int[][] m6 = strassensAlgorithm(m6a,m6b);
		
		//m7 = (a1 - a3)(b2 + b3)
		int[][] m7a = subtract(a1,a3);
		int[][] m7b = add(b2,b3);
		int[][] m7 = strassensAlgorithm(m7a,m7b);
		
		//Using only these 7 multiplications (instead of the usual 8) we can reconstruct c0-c3 as such
		//This reduction of multiplications is why the algorithm runs in sub-cubic time.
		//c0 = m1 + m4 -m5 +m7
		//c1 = m3 + m5
		//c2 = m2 + m4
		//c3 = m1 - m2 + m3 + m6
		
		//Now build the blocks of C with the results of m1-m7
		
		//c0 = m1 + m4 -m5 +m7
		int[][] tempA = add(m1,m4);
		int[][] tempB = add(tempA,m7);
		int[][] c0 = subtract(tempB,m5);
		
		//c1 = m3 + m5
		int[][] c1 = add(m3,m5);
		
		//c2 = m2 + m4
		int[][] c2 = add(m2,m4);
		
		//c3 = m1 - m2 + m3 + m6
		int[][] tempC = add(m3,m6);
		int[][] tempD = add(tempC,m1);
		int[][] c3 =  subtract(tempD,m2);
		
		//Reconstruct answer from the blocks
		int[][] C = new int[n][n];
		for (int i = 0; i < blockSize; i++) {
			for (int j = 0; j < blockSize; j++) {
				 C[i][j] = c0[i][j];
	             C[i][j + blockSize] = c1[i][j];
	             C[i + blockSize][j] = c2[i][j];
	             C[i + blockSize][j + blockSize] = c3[i][j];
			}
		}
		return C;
	    }
	}
	
	private static int[][] add(int[][] A, int[][] B) {
	    int n = A.length;
	    int[][] C = new int[n][n];
	    for (int i = 0; i < n; i++) {
	        for (int j = 0; j < n; j++) {
	            C[i][j] = A[i][j] + B[i][j];
	        }
	    }
	    return C;
	}

	private static int[][] subtract(int[][] A, int[][] B) {
	    int n = A.length;
	    int[][] C = new int[n][n];
	    for (int i = 0; i < n; i++) {
	        for (int j = 0; j < n; j++) {
	            C[i][j] = A[i][j] - B[i][j];
	        }
	    }
	    return C;
	}
	
	public static int[][] ikjAlgorithm(int[][] A, int[][] B) {
	    int n = A.length;

	    int[][] C = new int[n][n];

	    for (int i = 0; i < n; i++) {
	        for (int k = 0; k < n; k++) {
	            for (int j = 0; j < n; j++) {
	                C[i][j] += A[i][k] * B[k][j];
	            }
	        }
	    }
	    return C;
	}

